# README #

### What is this repository for? ###

* It's a fork of the KTreeMap library, taken from tag 1.1.0, in which we have removed references to java.awt.* classes
* Original code of the library is here: http://sourceforge.net/projects/jtreemap/
* This fork is being used by the Clover-for-Eclipse plugin https://www.atlassian.com/software/clover/download

### How do I get set up? ###

* Checkout source code into <workspace_dir>
* Install Eclipse, copy JAR files from <eclipse_install>/plugins to <workspace_dir>/target/eclipse
* Run 'mvn install'

### Contribution guidelines ###

* Fork repository, make your changes and create pull request to contribute

### Who do I talk to? ###

* Just raise an issue in this project or contact one of Clover developers (mparfianowicz@atlassian.com, glewandowski@atlassian.com).